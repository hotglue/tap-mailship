"""Stream type classes for tap-mailship."""

from pathlib import Path
from typing import Any, Dict, Iterable, List, Optional, Union

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_mailship.client import MailshipStream


class ProductsStream(MailshipStream):
    """Define custom stream."""

    name = "products"
    path = "/product/list"
    rest_method = "POST"
    primary_keys = ["id"]
    replication_key = "changedAt"
    # replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("eshops", th.CustomType({"type": ["array", "string"]})),
        th.Property("productSku", th.StringType),
        th.Property("internalSku", th.StringType),
        th.Property("organisation", th.StringType),
        th.Property("productGroup", th.StringType),
        th.Property("name", th.StringType),
        th.Property("description", th.StringType),
        th.Property("images", th.CustomType({"type": ["array", "string"]})),
        th.Property("intImages", th.CustomType({"type": ["array", "string"]})),
        th.Property("weight", th.NumberType),
        th.Property("height", th.NumberType),
        th.Property("width", th.NumberType),
        th.Property("length", th.NumberType),
        th.Property("intWeight", th.NumberType),
        th.Property("intHeight", th.NumberType),
        th.Property("intWidth", th.NumberType),
        th.Property("intLength", th.NumberType),
        th.Property("referenceNumbers", th.CustomType({"type": ["array", "string"]})),
        th.Property("packagingType", th.StringType),
        th.Property("workAroundSnIn", th.BooleanType),
        th.Property("workAroundEanSticker", th.BooleanType),
        th.Property("workAroundWarrantyInf", th.BooleanType),
        th.Property("workAroundLot", th.BooleanType),
        th.Property("active", th.BooleanType),
        th.Property("createdAt", th.DateTimeType),
        th.Property("changedAt", th.DateTimeType),
        th.Property("countryOfOrigin", th.StringType),
        th.Property("hsCode", th.StringType),
        th.Property("type", th.StringType),
        th.Property("category", th.StringType),
    ).to_dict()


class WarehouseStream(MailshipStream):
    """Define custom stream."""

    name = "warehoue_list"
    path = "/warehouse/list"
    rest_method = "POST"
    primary_keys = ["id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("customId", th.StringType),
        th.Property("wms", th.CustomType({"type": ["array", "string"]})),
        th.Property("defaultWms", th.StringType),
        th.Property("organisation", th.StringType),
        th.Property("createdAt", th.DateTimeType),
        th.Property("changedAt", th.DateTimeType),
    ).to_dict()


class ProductStockStream(MailshipStream):
    """Define custom stream."""

    name = "product_stock"
    path = "/product-stock/list"
    rest_method = "POST"
    primary_keys = ["id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("product", th.StringType),
        th.Property("warehouse", th.StringType),
        th.Property("wms", th.StringType),
        th.Property("quantity", th.NumberType),
        th.Property("booked", th.NumberType),
        th.Property("bookedAtExpeditions", th.NumberType),
        th.Property("bookedAtTransfers", th.NumberType),
        th.Property("bookedAtStockChanges", th.NumberType),
        th.Property("available", th.NumberType),
        th.Property("missing", th.NumberType),
        th.Property("missingAtExpeditions", th.NumberType),
        th.Property("missingAtTransfers", th.NumberType),
        th.Property("missingAtStockChanges", th.NumberType),
        th.Property("required", th.NumberType),
        th.Property("incoming", th.NumberType),
        th.Property(
            "lots",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.StringType),
                    th.Property("lot", th.StringType),
                    th.Property("date", th.DateTimeType),
                    th.Property("quantity", th.NumberType),
                    th.Property("booked", th.NumberType),
                    th.Property("available", th.NumberType),
                    th.Property("missing", th.StringType),
                    th.Property("required", th.NumberType),
                    th.Property("createdAt", th.DateTimeType),
                    th.Property("changedAt", th.DateTimeType),
                )
            ),
        ),
        th.Property("createdAt", th.DateTimeType),
        th.Property("changedAt", th.DateTimeType),
    ).to_dict()


class SuppliersStream(MailshipStream):
    """Define custom stream."""

    name = "suppliers"
    path = "/supplier/list"
    rest_method = "POST"
    primary_keys = ["id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("organisation", th.StringType),
        th.Property("name", th.StringType),
        th.Property("companyName", th.StringType),
        th.Property("firstName", th.StringType),
        th.Property("lastName", th.StringType),
        th.Property("degree", th.StringType),
        th.Property("degree2", th.StringType),
        th.Property("street", th.StringType),
        th.Property("houseNr", th.StringType),
        th.Property("city", th.StringType),
        th.Property("zip", th.StringType),
        th.Property("country", th.StringType),
        th.Property("phone", th.StringType),
        th.Property("email", th.StringType),
        th.Property("registrationNumber", th.StringType),
        th.Property("note", th.StringType),
        th.Property("active", th.BooleanType),
        th.Property("ref1", th.StringType),
        th.Property("ref2", th.StringType),
        th.Property("ref3", th.StringType),
        th.Property("createdAt", th.DateTimeType),
        th.Property("changedAt", th.DateTimeType),
    ).to_dict()


class ProductGroupsStream(MailshipStream):
    """Define custom stream."""

    name = "product_groups"
    path = "/product-group/list"
    rest_method = "POST"
    primary_keys = ["id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("createdAt", th.DateTimeType),
        th.Property("changedAt", th.DateTimeType),
    ).to_dict()


class ExpeditionsStream(MailshipStream):
    """Define custom stream."""

    name = "expeditions"
    path = "/expedition/list"
    rest_method = "POST"
    primary_keys = ["id"]
    replication_key = "changedAt"
    select = ["id", "createdAt", "changedAt"]
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("createdAt", th.DateTimeType),
        th.Property("changedAt", th.DateTimeType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        return {"expedition_id": record["id"]}


class ExpeditionItemsStream(MailshipStream):
    """Define custom stream."""

    name = "expedition_items"
    parent_stream_type = ExpeditionsStream
    path = "/expedition/{expedition_id}"
    rest_method = "GET"
    primary_keys = ["id"]
    records_jsonpath = "$[*]"
    replication_key = "createdAt"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("user", th.StringType),
        th.Property("eshop", th.StringType),
        th.Property("warehouse", th.StringType),
        th.Property("wms", th.StringType),
        th.Property("partner", th.StringType),
        th.Property("orderNumber", th.StringType),
        th.Property("note", th.StringType),
        th.Property("billingFirstName", th.StringType),
        th.Property("billingLastName", th.StringType),
        th.Property("billingDegree", th.StringType),
        th.Property("billingCompany", th.StringType),
        th.Property("billingStreet", th.StringType),
        th.Property("billingHouseNr", th.StringType),
        th.Property("billingZip", th.StringType),
        th.Property("billingCountry", th.StringType),
        th.Property("billingEmail", th.StringType),
        th.Property("billingPhone", th.StringType),
        th.Property("billingRegistrationNumber", th.StringType),
        th.Property("billingVatNumber", th.StringType),
        th.Property("differentDeliveryAddress", th.BooleanType),
        th.Property("deliveryFirstName", th.StringType),
        th.Property("deliveryLastName", th.StringType),
        th.Property("deliveryDegree", th.StringType),
        th.Property("deliveryCompany", th.StringType),
        th.Property("deliveryStreet", th.StringType),
        th.Property("deliveryHouseNr", th.StringType),
        th.Property("deliveryZip", th.StringType),
        th.Property("deliveryCity", th.StringType),
        th.Property("deliveryCountry", th.StringType),
        th.Property("deliveryEmail", th.StringType),
        th.Property("deliveryPhone", th.StringType),
        th.Property("requiredExpeditionDate", th.StringType),
        th.Property("carrier", th.StringType),
        th.Property("carrierService", th.StringType),
        th.Property("carrierPickupPlace", th.StringType),
        th.Property("carrierNote", th.StringType),
        th.Property("trackingNumber", th.StringType),
        th.Property("trackingUrl", th.StringType),
        th.Property("externalTrackingNumber", th.StringType),
        th.Property("packagesCount", th.IntegerType),
        th.Property("status", th.StringType),
        th.Property("expeditedCompletely", th.BooleanType),
        th.Property("value", th.NumberType),
        th.Property("currency", th.StringType),
        th.Property("fragile", th.BooleanType),
        th.Property("cod", th.BooleanType),
        th.Property("codValue", th.NumberType),
        th.Property("codCurrency", th.StringType),
        th.Property("codVariableSymbol", th.StringType),
        th.Property("customerGroup", th.StringType),
        th.Property("eshopOrderDate", th.StringType),
        th.Property("countOfItems", th.NumberType),
        th.Property("countOfSku", th.NumberType),
        th.Property("sumOfQuantity", th.NumberType),
        th.Property("packedAt", th.DateTimeType),
        th.Property("sentAt", th.DateTimeType),
        th.Property("deliveredAt", th.DateTimeType),
        th.Property("waitBeforeProcessing", th.BooleanType),
        th.Property("editBeforeProcessing", th.BooleanType),
        th.Property("priority", th.IntegerType),
        th.Property("ref1", th.StringType),
        th.Property("ref2", th.StringType),
        th.Property("ref3", th.StringType),
        th.Property("foreignPrice", th.CustomType({"type": ["object", "string"]})),
        th.Property("conversionDate", th.DateTimeType),
        th.Property("modifiedAt", th.DateTimeType),
        th.Property("invoiceUrl", th.StringType),
        th.Property("invoiceOriginalName", th.StringType),
        th.Property("withComplaint", th.BooleanType),
        th.Property("createdAt", th.DateTimeType),
        th.Property("changedAt", th.DateTimeType),
        th.Property("carrierPickupPlaceCode", th.StringType),
        th.Property("invoice", th.CustomType({"type": ["object", "string"]})),
        th.Property("items", th.CustomType({"type": ["array", "string"]})),
        th.Property("errors", th.CustomType({"type": ["array", "string"]})),
        th.Property("audits", th.CustomType({"type": ["array", "string"]})),
        th.Property("foreignPrice", th.CustomType({"type": ["object", "string"]})),
    ).to_dict()


class StockAdviceStream(MailshipStream):
    """Define custom stream."""

    name = "stock_advice"
    path = "/stock-advice/list"
    rest_method = "POST"
    primary_keys = ["id"]
    replication_key = "changedAt"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("user", th.StringType),
        th.Property("organisation", th.StringType),
        th.Property("warehouse", th.StringType),
        th.Property("warehouse", th.StringType),
        th.Property("wms", th.StringType),
        th.Property("supplier", th.StringType),
        th.Property("packagingUnit", th.StringType),
        th.Property("countOfUnits", th.NumberType),
        th.Property("status", th.StringType),
        th.Property("manualInput", th.BooleanType),
        th.Property("note", th.StringType),
        th.Property("internalId", th.StringType),
        th.Property("wmsInternalId", th.StringType),
        th.Property("mailwiseInternalId", th.StringType),
        th.Property("countOfItems", th.NumberType),
        th.Property("countOfSku", th.NumberType),
        th.Property("sumOfQuantity", th.NumberType),
        th.Property("sumOfSuppliedQuantity", th.NumberType),
        th.Property("ref1", th.StringType),
        th.Property("ref2", th.StringType),
        th.Property("ref3", th.StringType),
        th.Property("expectedAt", th.DateTimeType),
        th.Property("createdAt", th.DateTimeType),
        th.Property("changedAt", th.DateTimeType),
    ).to_dict()


class StockAdviceItemsStream(MailshipStream):
    name = "stock_advice_items"
    path = "/stock-advice-item/list"
    rest_method = "POST"
    primary_keys = ["id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("stockAdvice", th.StringType),
        th.Property("bookStockAdvices", th.CustomType({"type": ["array", "string"]})),
        th.Property("blocked", th.NumberType),
        th.Property("position", th.NumberType),
        th.Property("product", th.StringType),
        th.Property("quantity", th.NumberType),
    ).to_dict()
