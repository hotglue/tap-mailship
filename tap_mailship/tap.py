"""Mailship tap class."""

from pathlib import PurePath
from typing import List, Optional, Union

from singer_sdk import Stream, Tap
from singer_sdk import typing as th  # JSON schema typing helpers

# TODO: Import your custom stream types here:
from tap_mailship.streams import (
    ExpeditionItemsStream,
    ExpeditionsStream,
    ProductGroupsStream,
    ProductsStream,
    ProductStockStream,
    SuppliersStream,
    WarehouseStream,
    StockAdviceStream,
    StockAdviceItemsStream,
)

# TODO: Compile a list of custom stream types here
#       OR rewrite discover_streams() below with your custom logic.
STREAM_TYPES = [
    ProductsStream,
    WarehouseStream,
    ProductStockStream,
    SuppliersStream,
    ProductGroupsStream,
    ExpeditionsStream,
    ExpeditionItemsStream,
    StockAdviceStream,
    StockAdviceItemsStream,
]


class TapMailship(Tap):
    """Mailship tap class."""

    name = "tap-mailship"
    
    def __init__(
        self,
        config=None,
        catalog=None,
        state=None,
        parse_env_config=False,
        validate_config=True,
    ) -> None:
        super().__init__(config, catalog, state, parse_env_config, validate_config)
        self.config_file = config[0]


    # TODO: Update this section with the actual config values you expect:
    config_jsonschema = th.PropertiesList(
        th.Property(
            "username",
            th.StringType,
            required=True,
        ),
        th.Property("password", th.StringType, required=True),
        th.Property("start_date", th.DateTimeType),
        th.Property("token", th.StringType),
        th.Property("expires_in", th.IntegerType),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapMailship.cli()
