"""REST client handling, including MailshipStream base class."""

from pathlib import Path
from time import time
from typing import Any, Callable, Dict, Generator, Iterable, List, Optional, Union

import backoff
import requests
from requests.exceptions import JSONDecodeError
from memoization import cached
from singer_sdk.authenticators import BearerTokenAuthenticator
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream

SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class MailshipStream(RESTStream):
    """Mailship stream class."""

    access_token = None
    expires_in = None
    url_base = "https://app.mailship.eu/api"
    _page_size = 200
    offset = 0

    records_jsonpath = "$.results[*]"  # Or override `parse_response`.
    next_page_token_jsonpath = "$.next_page"  # Or override `get_next_page_token`.

    def get_token(self):
        # TODO: Move get token to inside the authenticator class
        expires_in = self.expires_in
        now = round(time())
        if not self.access_token or (not expires_in) or ((expires_in - now) < 60):
            s = requests.Session()
            payload = {
                "login": self.config.get("username"),
                "password": self.config.get("password"),
            }
            login = s.post(f"{self.url_base}/login/user", json=payload).json()
            self.access_token = login["token"]
            self.expires_in = login["exp"]
        return self.access_token

    @property
    def authenticator(self):
        """Return a new authenticator object."""
        if self.config.get("username") and self.config.get("password") is not None:
            token = self.get_token()
        return BearerTokenAuthenticator.create_for_stream(self, token=token)

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        self.validate_response(response)
        res = response.json()
        if "results" not in res:
            return None

        if len(res["results"]) > 0:
            self.offset = self.offset + self._page_size
            return self.offset
        else:
            return None

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if next_page_token:
            params["page"] = next_page_token
        return params

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:

        if self.rest_method is "GET":
            return None

        payload = {}
        payload["limit"] = self._page_size
        payload["offset"] = self.offset
        if payload["offset"] > 10:
            aa = ""
        payload["criteria"] = {}
        replication_value = self.get_starting_timestamp(context)
        try:
            payload["select"] = self.select
        except:
            pass

        if replication_value is not None:
            replication_value = replication_value.strftime("%Y-%m-%dT%H:%M:%SZ")
            payload["criteria"].update(
                {f"{self.replication_key}": {"gte": replication_value}}
            )

        return payload

    def get_records(self, context: Optional[dict]) -> Iterable[Dict[str, Any]]:
        for record in self.request_records(context):
            transformed_record = self.post_process(record, context)
            if transformed_record is None:
                # Record filtered out during post_process()
                continue
            yield transformed_record

    def post_process(self, row: dict, context: Optional[dict] = None) -> Optional[dict]:
        if "changedAt" in row:
            if row["changedAt"] is None:
                row["changedAt"] = row["createdAt"]
        return row

    def validate_response(self, response) -> None:
        # Tries to fetch json content
        try:
            response.json()
            valid_json = True
        except JSONDecodeError:
            valid_json = False

        if response.status_code == 200 and not valid_json:
            # We are getting an invalid response probably due to authorization
            # issues, so we try to generate a new token and retry the request
            self.get_token()
            self.logger.info(
                f"Returned 200 with content {response.content}, trying to generate a new token."
            )
            raise RetriableAPIError("Retrying due to bad auth", response)

        elif 500 <= response.status_code < 600:
            msg = self.response_error_message(response)
            raise RetriableAPIError(msg, response)

        elif response.status_code == 401:
            msg = self.response_error_message(response)
            self.get_token()
            self.logger.info(
                f"Returned 401 with message {msg}, getting a new token."
            )

        elif 400 <= response.status_code < 500:
            msg = self.response_error_message(response)
            raise FatalAPIError(msg)